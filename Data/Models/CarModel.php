<?php declare(strict_types=1);

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Models;

class CarModel extends AbstractModel
{
    /**
     * @var int
     */
    public $Message_ID;

    /**
     * @var string
     */
    public $name;

    /**
     * @var float
     */
    public $price;

    /**
     * @var int
     */
    public $stock;

    /**
     * @var int
     */
    public $Subdivision_ID;

    /**
     * @var string
     */
    public $Subdivision_IDS;

    /**
     * @var int|string
     */
    public $currency;

    /**
     * @var string
     */
    public $vendor;

    /**
     * @var string
     */
    public $text;

    /**
     * @var string
     */
    public $LastUpdated;

    /**
     * @var int|string
     */
    public $view;
}