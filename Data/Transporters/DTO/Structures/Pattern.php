<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures;

class Pattern
{
    /**
     * @var string
     */
    public $pattern;

    /**
     * @param string
     * 
     * @return self
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    public static function fromArray($data)
    {
        $pattern = new Pattern();

        $pattern->pattern = $data["pattern"];

        return $pattern;
    }
}