<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/vars.inc.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/connect_io.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/modules/default/function.inc.php";

use App\modules\Korzilla\YMLSection\CarsYML\UI\WEB\Controllers\GetSettingsFormController;

try {
    $controller = new GetSettingsFormController();

    $controller->execute();
} catch (Exception $e) {
    echo ''. $e->getMessage() .'';
}