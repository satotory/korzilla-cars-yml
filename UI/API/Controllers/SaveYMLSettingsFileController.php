<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\UI\API\Controllers;

use App\modules\Korzilla\YMLSection\CarsYML\Actions\SubActions\SaveYMLSettingsSubAction;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\CategoriesCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\CitiesCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\PatternsCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\City;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\Files;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Inputs\SaveYMLSettingsFileInput;

class SaveYMLSettingsFileController
{
    /**
     * @var SaveYMLSettingsSubAction
     */
    private $saveAction;

    public function __construct()
    {
        $this->saveAction = new SaveYMLSettingsSubAction();
    }

    /**
     * @var array $requestData
     */
    public function execute($requestData)
    {
        $requestData['categories'] = array_keys($requestData['categories']);

        $input                  = new SaveYMLSettingsFileInput;
        $input->settings        = YMLSettingsDTO::fromArray($requestData);

        try {
            $this->saveAction->run($input);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    private function _categories($categoriesArray)
    {
        return array_keys($categoriesArray);
    }

    /**
     * @param array|mixed
     * 
     * @return City[]|null
     */
    private function _cities($citiesIdsArray)
    {
        foreach ($citiesIdsArray as $cityId) {
            $cities[] = City::fromArray($cityId);
        }

        return $cities ?? null;
    }
}