<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Constants;

class ListOfAvailableDefaultValues
{
    const CONDITION = [
        "active"    => false,
        "type"      => "select",
        "name"      => "Состояние",
        "value"     => 3,
        "defaults"  => [
            1 => "Не на ходу",
            2 => "Нуждается в ремонте",
            3 => "Не требует ремонта",
        ],
    ];
    const MILEAGE   = [
        "active"    => false,
        "type"      => "input",
        "title"     => "(Число. В километрах.)",
        "name"      => "Пробег",
        "value"     => 0,
    ];

    /**
     * @return array
     */
    public static function list()
    {
        return [
            "condition" => self::CONDITION,
            "mileage"   => self::MILEAGE,
        ];
    }

    /**
     * @return mixed|null
     */
    public static function getValueByKey($key, $valueId)
    {
        
        $dv = self::list()[$key] ?? null;
        if ($dv === null) {
            return null;
        }

        if ($dv['type'] != "select") {
            return $valueId;
        }

        return $dv["defaults"][$valueId];
    }
}