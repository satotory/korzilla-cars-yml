<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Constants;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\City;

class ListOfAvailablePatterns
{
    const CATEGORY      = "%CATEGORY%";
    const SUBDOMAIN     = "%SUBDOMAIN%";
    const CITY_NAME_GDE  = "%CITYNAME-GDE%";

    /**
     * @return array
     */
    public static function list()
    {
        return array_keys(self::listWithMethods());
    }

    public static function getMethodForPattern($pattern)
    {
        $defaultClosure = (function ($v) {
            return '';
        });

        return self::listWithMethods()[$pattern] ?? $defaultClosure;
    }

    private static function listWithMethods()
    {
        return [
            self::CATEGORY => (function ($v) {
                return $v;
            }),
            self::CITY_NAME_GDE => (function (City $city) {
                return "в " . cityGde($city->name);
            }),
            self::SUBDOMAIN => (function (City $city) {
                return $city;
            }),
        ];
    }
}