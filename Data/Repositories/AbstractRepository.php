<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Repositories;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Models\AbstractModel;
use nc_Db;

abstract class AbstractRepository
{
    /**
     * @var nc_Db
     */
    protected $db;

    public function __construct(nc_Db $db) 
    {
        $this->db = $db;
    }
    
    abstract protected function getTableName() : string;

    abstract protected function getModelClassName() : string;

    /**
     * @return ?AbstractModel
     */
    public function get(int $id)
    {
        $sql = sprintf("SELECT * FROM `%s` WHERE %s = %d", 
            $this->getTableName(),
            $this->mapPropertyToDb('id'),
            $id
        );

        if (!$row = $this->db->get_row($sql)) {
            return null;
        }

        return $this->mapRowToModel($row);
    }

    protected function mapPropertyToDb(string $property): string
    {
        return sprintf('%s.`%s`', $this->getTableName(), $property);
    }

    protected function mapValueToDb($value): string
    {
        if ($value === null) {
            return 'NULL';
        }

        if (is_int($value)) {
            return "{$value}";
        }

        return "'{$value}'";
    }

    protected function mapRowToModel(\stdClass $row): AbstractModel
    {
        $model = $this->getModel();

        foreach ($row as $field => $value) {
            $model->$field = $value;
        }

        return $model;
    }

    protected function getModel(): AbstractModel
    {
        $class = $this->getModelClassName();

        if (!is_subclass_of($class, AbstractModel::class, true)) {
            throw new \Exception('модель должна наследоваться от AbstractModel');
        }

        return new $class;
    }

}