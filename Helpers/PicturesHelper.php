<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Helpers;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\Picture;
use Class2001;

class PicturesHelper
{
    /**
     * @param int|string $id
     * 
     * @return null|Picture[]
     */
    public static function __getProductPicturesById($id)
    {
        $product = Class2001::getItemById($id);
        
        if (!$product || ! $pictures = $product->getPhoto()) {
            return null;
        }
        
        foreach ($pictures as $i => $picture) {
            $pic = new Picture;
            $pic->path = $picture['path'];
            
            $pictures[$i] = $pic;
        }

        return $pictures;
    }
}