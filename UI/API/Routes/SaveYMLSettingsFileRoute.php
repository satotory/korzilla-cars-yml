<?php
use App\modules\Korzilla\YMLSection\CarsYML\UI\API\Controllers\SaveYMLSettingsFileController;

require_once $_SERVER['DOCUMENT_ROOT'] . "/vars.inc.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/connect_io.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/modules/default/function.inc.php";

// TODO:
    // [] сохранение настроек выгрузки в файл
        // [X] id
        // [X] разделы
        // [X] города для которых нужно делать юмл
        // [] паттерны:
            // [X] паттерны для сетов

try {
    if (!$_POST['categories'] || !$_POST['id']) {
        throw new Exception('REQUEST_DATA_ERROR_CODE');
    }

    $controller = new SaveYMLSettingsFileController();

    $controller->execute($_POST);
    
    echo json_encode([
        "title" => "ОК",
        "modal" => "close",
        "reload" => "1",
        "succes" =>  "настройки сохранены"
    ]);
} catch (Exception $e) {
    echo ''. $e->getMessage() .'';
}