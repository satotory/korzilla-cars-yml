<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\CategoriesCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\CitiesCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\DefaultValuesCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\PatternsCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\City;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\Files;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\Pattern;

class YMLSettingsDTO
{
    const PLATFORM = 'Korzilla';

    /**
     * @var string
     */
    public $id;

    /**
     * @var CategoriesCollection
     */
    public $categories;

    /**
     * @var PatternsCollection
     */
    public $patterns;

    /**
     * @var CitiesCollection
     */
    public $cities;

    /**
     * @var Files|null
     */
    public $files;

    /**
     * @var DefaultValuesCollection
     */
    public $defaultValues;

    public function __construct()
    {
        $this->categories    = new CategoriesCollection([]);
        $this->cities        = new CitiesCollection([]);
        $this->patterns      = new PatternsCollection([]);
        $this->files         = new Files([]);
        $this->defaultValues = new DefaultValuesCollection([]);
    }

    /**
     * @param array|mixed data
     */
    public static function fromArray($data)
    {
        $settings = new YMLSettingsDTO();
        
        $settings->id            = $data['id'];
        $settings->files         = new Files($data['files'] ?? []);
        $settings->cities        = new CitiesCollection($data['cities'] ?? []);
        $settings->patterns      = new PatternsCollection($data['patterns'] ?? []);
        $settings->categories    = new CategoriesCollection($data['categories'] ?? []);
        $settings->defaultValues = new DefaultValuesCollection($data['defaultValues'] ?? []);

        return $settings;
    }

    /**
     * @return Currency
     */
    public static function _getCurrencyById($id)
    {
        $currencies = self::_getCurrencies();
        
        return $currencies[$id] ?? $currencies[1];
    }
    /**
     * @return Currency[]
     */
    public static function _getCurrencies()
    {
        $RUR        = new Currency;
        $RUR->id    = 'RUR';
        $RUR->rate  = '1';

        return [
            "1" => $RUR,
        ];
    }

    public static function _getYMLsPatternsFolder()
    {
        global $DOCUMENT_ROOT, $pathInc;
        
        return self::___check_dir($DOCUMENT_ROOT . $pathInc ."/yml/import/CarsYML/patterns/");
    }

    /**
     * @param string
     * 
     * возвращает относительный DOCUMENT_ROOT путь
     * @return string
     */
    public static function _getRelativeFilePath(string $path)
    {
        global $DOCUMENT_ROOT;

        return str_replace($DOCUMENT_ROOT, '', $path);
    }
    
    public static function _getYMLsCitiesFolder($cityId = 0)
    {
        global $DOCUMENT_ROOT, $pathInc;
        
        return self::___check_dir($DOCUMENT_ROOT . $pathInc ."/yml/import/CarsYML/YMLs/{$cityId}/");
    }

    /**
     * @param string|int $YMLId
     */
    public static function _getSettingsFile($YMLId)
    {
        global $DOCUMENT_ROOT, $pathInc;

        return self::___check_dir(
            $DOCUMENT_ROOT . $pathInc ."/yml/import/CarsYML/settings/"
        )."{$YMLId}.json";
    }

    public static function _getSettingsFolder()
    {
        global $DOCUMENT_ROOT, $pathInc;

        return self::___check_dir($DOCUMENT_ROOT . $pathInc ."/yml/import/CarsYML/settings/");
    }

    private static function ___check_dir(string $dir)
    {
        try {
            if (!file_exists($dir) || !is_dir($dir)) {
                mkdir($dir, 0777, true);
                
                if (!file_exists($dir) || !is_dir($dir)) {
                    throw new \Exception('CREATING_DIR_ERROR_CODE');
                }
            }
    
            return $dir;
        }  catch (\Exception $e) {
            die($e->getMessage());
        }
    }
}

class Currency
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $rate;
}