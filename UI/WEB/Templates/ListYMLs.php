<?php
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;
use App\modules\Korzilla\YMLSection\CarsYML\Helpers\FileHelper;
?>
<script src="<?= str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__) . '/sources/main.js?time=' . time() ?>"></script>
<link href="<?= str_replace($_SERVER['DOCUMENT_ROOT'], '', __DIR__) . '/sources/style.css?time=' . time() ?>" rel="stylesheet">

<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/vars.inc.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/connect_io.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/modules/default/function.inc.php";

global $nc_core;

$linkListYMLs = "/bc/modules/Korzilla/YMLSection/CarsYML/UI/WEB/Templates/ListYMLs.php";
$linkToCreateNewYMLFile = '/bc/modules/Korzilla/YMLSection/CarsYML/UI/WEB/Routes/GetSettingsFormRoute.php';
$linkToBuildYML         = '/bc/modules/Korzilla/YMLSection/CarsYML/UI/API/Routes/BuildCarsYMLRoute.php';

$YMLsSettingsFolder = YMLSettingsDTO::_getSettingsFolder();
$YMLsSettingsFiles = array_diff(
    scandir($YMLsSettingsFolder), 
    array('.', '..')
);

?>

<script>
    async function updateYML(o) {
        console.log(o.dataset)

        fetch(o.dataset.href)
            .then((response) => {
                return response.text();
            })
            .then(() => {
                fetch('<?= $linkListYMLs ?>')
                    .then((response) => {
                        return response.text();
                    }).then((html) => {
                        document.getElementById('cars_yml').innerHTML = html;
                    }).catch(() => {
                        location.reload();
                    })
            })

        
    }
</script>
<div class="cars_yml" id="cars_yml">
    <div class="view-body-inline export-yml">
        <div class="v-title">
            <a href="<?= $linkToCreateNewYMLFile ?>" 
                class="add-btn" 
                title="Добавление прайса" 
                data-rel="lightcase" 
                data-lc-options='{"maxWidth":950,"groupClass":"modal-edit modal-export-excel"}'>
                Создать YML-файл
            </a>
        </div>
        <div class="v-body">
            <?php foreach ($YMLsSettingsFiles as $YMLSettingsFile) : ?>
                
                <?php

                $settings = YMLSettingsDTO::fromArray(
                    FileHelper::__parseFileContent($YMLsSettingsFolder . $YMLSettingsFile)
                );

                ?>
                    <a  href='javascript:void(0)'
                        onclick="updateYML(this)"
                        data-href="<?= $linkToBuildYML ?>?id=<?= $settings->id ?>" 
                        class="" style='text-decoration:none;' 
                        title="Обновить YML '<?= $settings->id ?>'"
                        >
                        Обновить YML
                    </a>
                    <div class="v-line" data-id="<?= $settings->id ?>">
                        <div class="v-line-param">
                            <div class="v-second">
                                <a href="<?= $linkToCreateNewYMLFile ?>?id=<?= $settings->id ?>" 
                                    class="v-setting" 
                                    title="Настройки YML '<?= $settings->id ?>'" 
                                    data-rel="lightcase" 
                                    data-lc-options='{"maxWidth":950,"groupClass":"modal-edit modal-export-excel"}'>
                                </a>
                            </div>
                        </div>
                        <div class="v-inline v-line-info">
                            <table>
                                <tbody>
                                    <tr class="name">
                                        <td class="name">YML:</td>
                                        <td class="value"><?= $settings->id ?></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?php if ($settings->files->ymlFiles != null) : ?>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="name">Обновлено:</td>
                                        <td class="value">
                                            <?= date("d.m.Y H:i", $settings->files->patternFile->lastModified) ?>
                                        </td>
                                    </tr>
                                    <tr class="pattern_row">
                                        <td class="name">Шаблон:</td>
                                        <td class="pattern_link">
                                            <a class="link" href="<?= ($settings->files->patternFile->relativeDestination) ?>" target="_blank">
                                                открыть
                                            </a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <details class="info cities" open>
                                <summary>Города</summary>
                                <table>
                                    <tbody>
                                        <?php foreach($settings->files->ymlFiles as $ymlFile) : ?>
                                            <tr class="city_row">
                                                <td class="city_name"><?= $ymlFile->city->name ?></td>
                                                <td class="yml_link">
                                                    <a class="link" href="<?= $ymlFile->relativeDestination ?>" target="_blank">
                                                        открыть
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </details>
                        </div>
                        <?php endif; ?>
                    </div>

                    
            <?php endforeach; ?>
        </div>
    </div>
</div>
