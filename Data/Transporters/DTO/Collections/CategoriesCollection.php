<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections;

class CategoriesCollection extends AbstractCollection
{
    /**
     * @param array<array>|mixed $array
     */
    public function __construct($array)
    {
        foreach ($array as $value) {
            $this->add($value);
        }
    }
}