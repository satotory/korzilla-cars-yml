<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Helpers;

class CityHelper
{
    /**
     * @return string|null
     */
    public static function __getCityNameByCityId($cityId)
    {
        global $setting;
        
        return $setting['lists_targetcity'][$cityId]['name'] ?? null;
    }

    public static function __getCityKeywordByCityId($cityId)
    {
        global $setting;

        return $setting['lists_targetcity'][$cityId]['keyword'] ?? null;
    }
}