<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Repositories;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Constants\SubClassIDsConstants;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Models\SubdivisionModel;

class SubdivisionRepository extends AbstractRepository
{

    /**
     * @param array $subs
     * 
     * @return SubdivisionModel|array
     */
    public function getByArrayOfIds_Iterable($subs)
    {
        $sql = sprintf(
            "SELECT Subdivision.*, 
                COUNT(Message2001.`Message_ID`) as Products_Count 
                FROM `Subdivision` 
                LEFT JOIN `Message2001` 
                    ON (
                        Subdivision.`Subdivision_ID`=Message2001.`Subdivision_ID` 
                        OR INSTR(Message2001.`Subdivision_IDs`, Subdivision.`Subdivision_ID`)
                        ) 
                WHERE Subdivision.`Subdivision_ID` IN (%s)
                        AND Message2001.`price` > 0
                GROUP BY Subdivision.`Subdivision_ID`
                HAVING Products_Count > 0",
            implode(',', $subs)
        );

        
        if (!$rows = $this->db->get_results($sql)) {
            var_dump($sql);
            var_dump($this->db->last_error);

            return [];
        }
        
        foreach ($rows as $row) {
            yield $this->mapRowToModel($row);
        }
    }

    /**
     * @return SubdivisionModel[]|array
     */
    public function getProductSubdivisionsByCatalogueID(int $catalogue_id)
    {
        $result = [];

        foreach ($this->getProductSubdivisionsByCatalogueID_Iterable($catalogue_id) as $model) {
            $result[] = $model;
        }

        return $result;
    }

    /**
     * @return SubdivisionModel|array
     */
    public function getProductSubdivisionsByCatalogueID_Iterable(int $catalogue_id)
    {
        $sql = sprintf("SELECT * FROM `%s` INNER JOIN `%s` ON %s=%s WHERE %s = %d AND %s = %d AND %s = %d ORDER BY %s", 
            $this->getTableName(),
            'Sub_Class',
            'Sub_Class.`Subdivision_ID`',
            $this->mapPropertyToDb('Subdivision_ID'),
            'Sub_Class.`Class_ID`',
            $this->mapValueToDb(SubClassIDsConstants::PRODUCTS_SUB_CLASS_ID),
            $this->mapPropertyToDb('Catalogue_ID'),
            $this->mapValueToDb($catalogue_id),
            $this->mapPropertyToDb('Checked'),
            $this->mapValueToDb(1),
            $this->mapPropertyToDb('Hidden_URL')
        );

        if (!$rows = $this->db->get_results($sql)) {
            return [];
        }

        foreach ($rows as $row) {
            yield $this->mapRowToModel($row);
        }
    }

    protected function getModelClassName() : string
    {
        return SubdivisionModel::class;
    }

    protected function getTableName() : string
    {
        return 'Subdivision';
    }
}