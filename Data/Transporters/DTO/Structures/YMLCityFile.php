<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures;

class YMLCityFile extends File
{
    /**
     * @var City
     */
    public $city;

    public static function fromArray(array $data)
    {
        $YMLCityFile = new YMLCityFile();
        
        $YMLCityFile->destination = $data['destination'];
        $YMLCityFile->city = City::fromArray($data['city']);
        $YMLCityFile->relativeDestination = $data['relativeDestination'];
        
        return $YMLCityFile;
    }
}