<?php
use App\modules\Korzilla\YMLSection\CarsYML\UI\API\Controllers\BuildCarsYMLController;

require_once $_SERVER['DOCUMENT_ROOT'] . "/vars.inc.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/connect_io.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/modules/default/function.inc.php";

// TODO:
    // [] создание юмл
        // [X] создание базы юмл файла
        // [X] создание паттерна
        // [X] создание юмл файлов для городов на основе паттерна

date_default_timezone_set("Europe/Moscow");

try {
    $controller = new BuildCarsYMLController();

    $controller->execute($_GET);
} catch (Exception $e) {
    echo ''. $e->getMessage() .'';
}