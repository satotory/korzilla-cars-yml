<?php declare(strict_types=1);

namespace App\modules\Korzilla\YMLSection\CarsYML\Actions;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Constants\ListOfAvailableDefaultValues;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Constants\ListOfAvailablePatterns;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Models\CarModel;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Models\SubdivisionModel;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Repositories\CarRepository;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Repositories\SubdivisionRepository;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\CatalogueDTO;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\City;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\File;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\YMLCityFile;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Inputs\BuildCarsYMLInput;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Outputs\BuildCarsYMLSuccessOutput;
use App\modules\Korzilla\YMLSection\CarsYML\Helpers\LinksHelper;
use App\modules\Korzilla\YMLSection\CarsYML\Helpers\PicturesHelper;
use XMLWriter;

class BuildCarsYMLAction
{
    /**
     * @var SubdivisionRepository
     */
    private $subdivisionRepository;

    /**
     * @var CarRepository
     */
    private $carRepository;

    public function __construct(
        CarRepository $carRepository,
        SubdivisionRepository $subdivisionRepository
    ) {
        $this->carRepository            = $carRepository;
        $this->subdivisionRepository    = $subdivisionRepository;
    }

    /**
     * @throws \Exception
     * 
     * @return BuildCarsYMLSuccessOutput
     */
    public function run(BuildCarsYMLInput $input)
    {
        try {
            $patternFile = $this->_buildPattern($input);
            
            $input->settings->files->patternFile = $patternFile;

            if (!$input->settings->cities->collection) {
                $input->settings->files->ymlFiles['default'] = $this->_createFileByPattern($input->settings);
            } else {
                foreach ($input->settings->cities->collection as $city) {
                    $input->settings->files->ymlFiles[$city->keyword] = $this->_createFileByPatternForCity($input->settings, $city);
                }
            }

            $output = new BuildCarsYMLSuccessOutput();
            $output->settings = $input->settings;
        } catch (\Exception $e) {
            die($e->getMessage());
        }

        return $output;
    }

    /**
     * @throws \Exception
     * 
     * @return File
     */
    private function _createFileByPattern(YMLSettingsDTO $settings)
    {
        $fileContent = $settings->files->patternFile->getContent();
        
        foreach (ListOfAvailablePatterns::list() as $pattern) {
            $fileContent = str_replace(
                $pattern, 
                '', 
                $fileContent
            );
        }
        
        $file = new File;
        $file->destination = YMLSettingsDTO::_getYMLsCitiesFolder('default') . $settings->id . ".xml";
        
        if (!file_put_contents($file->destination, $fileContent)) {
            throw new \Exception("SAVING_DEFAULT_CY_ERROR_CODE");
        }

        $file->relativeDestination = YMLSettingsDTO::_getRelativeFilePath($file->destination);
        $file->lastModified = time();

        return $file;
    }

    /**
     * поддерживает только таргетинг
     * 
     * @throws \Exception
     * 
     * @return YMLCityFile
     */
    private function _createFileByPatternForCity(YMLSettingsDTO $settings, City $city)
    {
        $cityFileContent = $settings->files->patternFile->getContent();

        $cityFileContent = str_replace(
            ListOfAvailablePatterns::CITY_NAME_GDE, 
            "в " . cityGde($city->name), 
            $cityFileContent
        );

        $cityFileContent = str_replace(
            ListOfAvailablePatterns::SUBDOMAIN, 
            $city->keyword . ".", 
            $cityFileContent
        );

        $cityFile       = new YMLCityFile;
        $cityFile->city = $city;
        
        $cityFile->destination  = YMLSettingsDTO::_getYMLsCitiesFolder($city->keyword) . $settings->id . ".xml";
        if (!file_put_contents($cityFile->destination, $cityFileContent)) {
            throw new \Exception("SAVING_{$city->id}_CY_ERROR_CODE");
        }

        $cityFile->relativeDestination = YMLSettingsDTO::_getRelativeFilePath($cityFile->destination);
        $cityFile->lastModified = time();

        return $cityFile;
    }

    /**
     * @throws \Exception
     * 
     * @return File
     */
    private function _buildPattern(BuildCarsYMLInput $input)
    {
        $settings   = &$input->settings;
        $catalogue  = &$input->catalogue;

        $xmlWriter = new XMLWriter();
        $xmlWriter->openMemory();
        $xmlWriter->setIndent(true);

        // document start
        $xmlWriter->startDocument('1.0', 'UTF-8', 'yes');
            // yml_catalog start
            $xmlWriter->startElement("yml_catalog");
            $xmlWriter->writeAttribute("date", date("Y-m-d H:i"));
                // shop start
                $xmlWriter->startElement("shop");
                    $this->_writeCatalogueInfo($xmlWriter, $catalogue);
                    
                    $xmlWriter->startElement("currencies");
                        $this->_writeCurrencies($xmlWriter);
                    $xmlWriter->endElement();

                    $xmlWriter->startElement("categories");
                        $categories = $this->_writeCategories($xmlWriter, $settings);
                    $xmlWriter->endElement();

                    $xmlWriter->startElement("sets");
                        $this->_writeSets($xmlWriter, $settings, $catalogue, $categories);
                    $xmlWriter->endElement();
 
                    $xmlWriter->startElement("offers");
                        $this->_writeOffers($xmlWriter, $input, $categories);
                    $xmlWriter->endElement();

                // shop end
                $xmlWriter->endElement(); 
            // yml_catalog end
            $xmlWriter->endElement();
        // document end
        $xmlWriter->endDocument();

        
        $patternFile                = new File;
        $patternFile->destination   = YMLSettingsDTO::_getYMLsPatternsFolder() . $settings->id . ".xml";
        $patternFile->setContent($xmlWriter->flush(true));
        
        if (!file_put_contents($patternFile->destination, $patternFile->getContent())) {
            throw new \Exception('BUILD_PATTERN_SAVE_ERROR_CODE');
        }

        $patternFile->relativeDestination = YMLSettingsDTO::_getRelativeFilePath($patternFile->destination);
        $patternFile->lastModified = time();

        return $patternFile;
    }

    private function _writeCatalogueInfo(XMLWriter $xmlWriter, CatalogueDTO $catalogue)
    {
        $xmlWriter->writeElement("name",     $catalogue->name);
        $xmlWriter->writeElement("company",  $catalogue->name);
        $xmlWriter->writeElement("url",      $catalogue->domain);
        $xmlWriter->writeElement("platform", YMLSettingsDTO::PLATFORM);
    }

    /**
     * @param SubdivisionModel[] $categories
     * 
     * @return
     */
    private function _writeOffers(XMLWriter $xmlWriter, BuildCarsYMLInput $input, $categories)
    {
        $writtenCarsIds = [];

        foreach ($categories as $category) {
            /** @var CarModel $car */
            foreach ($this->carRepository->getCarsByCategoryId_Iterable($category->Subdivision_ID) as $car) {
                
                if ($writtenCarsIds[$car->Message_ID]) {
                    continue;
                }

                $this->_writeOffer($xmlWriter, $input, $car, $categories);

                $writtenCarsIds[$car->Message_ID] = true;
            }
        }
    }

    private function _writeOffer(XMLWriter $xmlWriter, BuildCarsYMLInput $input, CarModel $car, $categories)
    {
        $setIds = $this->_getCarSetsFromCategories($car, $categories);
        
        $carCategoryId = $this->_getCarCategoryId($car, $categories, $setIds);
        if ($carCategoryId == null) {
            return;
        }
        
        $carPictures = PicturesHelper::__getProductPicturesById($car->Message_ID);
        if ($carPictures == null) {
            return;
        }
        
        $xmlWriter->startElement("offer");

            $xmlWriter->writeAttribute("id", (string) $car->Message_ID);
            
            $xmlWriter->writeElement("name", htmlspecialchars_decode($car->name));
            
            if (count($setIds) > 0) {
                $xmlWriter->writeElement("set-ids", implode(", ", $setIds));
            }
            
            $xmlWriter->writeElement("categoryId", $carCategoryId);

            $xmlWriter->writeElement("url",
                $this->_getUrlPattern(
                    $input->catalogue->domain, 
                    LinksHelper::__getProductURLById($car->Message_ID)
                )
            );

            $xmlWriter->writeElement("price", (string) $car->price);
            $xmlWriter->writeElement(
                "currencyId", 
                (string) YMLSettingsDTO::_getCurrencyById($car->currency)->id
            );
            $xmlWriter->writeElement(
                "vendor",
                $car->vendor ?? $input->catalogue->name
            );

            foreach ($carPictures as $picture) {
               if (!stristr($picture->path, "://")) {
                    $picture->path = $this->_getUrlPattern(
                        $input->catalogue->domain, 
                        $picture->path
                    );
               }

               $xmlWriter->writeElement("picture", $picture->path);
            }
            
            $xmlWriter->startElement("param");
                $xmlWriter->writeAttribute("name", "Конверсия");
                $xmlWriter->text($car->view);
            $xmlWriter->endElement();

            $xmlWriter->startElement("param");
                $xmlWriter->writeAttribute("name", "Число отзывов");
                $xmlWriter->text((string) ($car->Message_ID % 130));
            $xmlWriter->endElement();

            $xmlWriter->startElement("param");
                $xmlWriter->writeAttribute("name", "Опубликовано");
                $xmlWriter->text(date(DATE_ISO8601, strtotime($car->LastUpdated)));
            $xmlWriter->endElement();
            
            $this->_writeDefaultParams($xmlWriter, $input->settings);   
            
            if ($carDescription = $this->_getCarDescription($car)) {
                $xmlWriter->startElement("description");
                $xmlWriter->writeCData($carDescription);
                $xmlWriter->endElement();
            }


        $xmlWriter->endElement();
    }

    private function _writeDefaultParams(XMLWriter $xmlWriter, YMLSettingsDTO $settings)
    {
        if (!$settings->defaultValues) {
            return;
        }

        foreach ($settings->defaultValues->collection as $key => $dv) {
            if (!$dv->active) {
                continue;
            }
            $value = ListOfAvailableDefaultValues::getValueByKey($key, $dv->value);
            if ($value === null) {
                continue;
            }

            $xmlWriter->startElement("param");
                $xmlWriter->writeAttribute("name", $dv->name);
                $xmlWriter->text((string) $value);
            $xmlWriter->endElement();
        }
    }

    /**
     * @return null|string
     */
    private function _getCarDescription(CarModel $car)
    {
        $description = trim(
            htmlspecialchars(
                strip_tags(
                    preg_replace('/(?:|)/m', '', $car->text)
                )
            )
        );

        return $description ?? null;
    }

    /**
     * @param SubdivisionModel[] $setIds
     * @param array $setIds
     * 
     * @return string|null
     */
    private function _getCarCategoryId(CarModel $car, $categories, $setIds)
    {
        if (!$categories[$car->Subdivision_ID]) {
            if (count($setIds) == 0) {
                return null;
            }

            $carCategory = array_key_first($setIds);
        } else {
            $carCategory = $car->Subdivision_ID;
        }

        return (string) $carCategory;
    }

    /**
     * @param SubdivisionModel[] $categories
     * 
     * @return array
     */
    private function _getCarSetsFromCategories(CarModel $car, $categories)
    {
        $setIds = [];
        foreach ($categories as $set) {
            if ($set->getSetGuid() == null) {
                continue;
            }

            if ($set->Subdivision_ID == $car->Subdivision_ID
                || strpos($car->Subdivision_IDS, (string) $set->Subdivision_ID) !== false
            ) {
                $setIds[$set->Subdivision_ID] = (string) $set->getSetGuid();
            }
        }

        return $setIds;
    }

    /**
     * @param SubdivisionModel[] $categories
     * 
     * @return void
     */
    private function _writeSets(XMLWriter $xmlWriter, YMLSettingsDTO $settings, CatalogueDTO $catalogue, $categories)
    {
        $minimumProductsCountForSet = 4;

        foreach ($categories as $category) {
            if ($category->Products_Count < $minimumProductsCountForSet) {
                continue;
            }

            $category->setSetGuid((int) $category->Subdivision_ID);

            // set start
            $xmlWriter->startElement("set");

            $xmlWriter->writeAttribute("id", $category->getSetGuid());
            
            $xmlWriter->writeElement("name",
                $this->__replaceNeedleInText(
                    ListOfAvailablePatterns::CATEGORY, 
                    $settings->patterns->get('sets')->pattern, 
                    $category->Subdivision_Name
                )
            );

            $xmlWriter->writeElement("url", $this->_getUrlPattern($catalogue->domain, $category->Hidden_URL));

            // category end
            $xmlWriter->endElement();
        }
    }

    private function _getUrlPattern(string $domain, string $path): string
    {
        $url = sprintf("%s://%s%s%s",
            'https',
            ListOfAvailablePatterns::SUBDOMAIN,
            $domain,
            $path
        );
        
        return $url;
    }

    /**
     * @return array
     */
    private function _writeCategories(XMLWriter $xmlWriter, YMLSettingsDTO $settings)
    {
        $writtenCategories = [];

        foreach ($this->subdivisionRepository->getByArrayOfIds_Iterable(
            $settings->categories->collection) as $category) {
            
            // category start
            $xmlWriter->startElement("category");

            $xmlWriter->writeAttribute("id", (string) $category->Subdivision_ID);

            if ($category->Parent_Sub_ID > 0 && $writtenCategories[$category->Parent_Sub_ID]) {
                $xmlWriter->writeAttribute("parentId", (string) $category->Parent_Sub_ID);
            }

            $xmlWriter->text($category->Subdivision_Name);

            // category end
            $xmlWriter->endElement();

            $writtenCategories[$category->Subdivision_ID] = $category;
        }

        return $writtenCategories;
    }

    /**
     * @return void
     */
    private function _writeCurrencies(XMLWriter $xmlWriter)
    {
        foreach (YMLSettingsDTO::_getCurrencies() as $currency) {
            // currency start
            $xmlWriter->startElement("currency");
                $xmlWriter->writeAttribute("id",    $currency->id);
                $xmlWriter->writeAttribute("rate",  $currency->rate);

            // currency end
            $xmlWriter->endElement();
        }
    }

    private function __replaceNeedleInText($needle, $text, $replaceText)
    {
        return str_replace($needle, $replaceText, $text);
    }
}