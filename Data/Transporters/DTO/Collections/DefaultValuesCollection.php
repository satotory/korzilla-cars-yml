<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Constants\ListOfAvailableDefaultValues;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\DefaultValue;

class DefaultValuesCollection extends AbstractCollection
{
    /**
     * @var DefaultValue[]
     */
    public $collection;

    /**
     * @param array<array>|mixed $array
     */
    public function __construct($array)
    {
        foreach ($this->__requiredFields() as $key => $values) {
            foreach ($values as $k => $v) {
                if (!$array[$key][$k]) {
                    $array[$key][$k] = $values[$k];
                }
            }
            $this->add(
                DefaultValue::fromArray($array[$key] ?? $values),
                $key
            );
        }
    }

    /**
     * @return array
     */
    private function __requiredFields()
    {
        return ListOfAvailableDefaultValues::list();
    }
}