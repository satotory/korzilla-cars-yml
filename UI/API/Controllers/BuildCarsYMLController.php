<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\UI\API\Controllers;

use App\modules\Korzilla\YMLSection\CarsYML\Actions\BuildCarsYMLAction;
use App\modules\Korzilla\YMLSection\CarsYML\Actions\SubActions\SaveYMLSettingsSubAction;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Repositories\CarRepository;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Repositories\SubdivisionRepository;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\CatalogueDTO;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Inputs\BuildCarsYMLInput;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Inputs\SaveYMLSettingsFileInput;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Outputs\BuildCarsYMLSuccessOutput;
use App\modules\Korzilla\YMLSection\CarsYML\Helpers\FileHelper;

class BuildCarsYMLController
{
    /**
     * @var SaveYMLSettingsSubAction
     */
    private $saveAction;

    /**
     * @var BuildCarsYMLAction
     */
    private $buildAction;

    public function __construct()
    {
        $core   = \nc_Core::get_object();

        $this->saveAction = new SaveYMLSettingsSubAction();
        $this->buildAction = new BuildCarsYMLAction(
            new CarRepository($core->db),
            new SubdivisionRepository($core->db)
        );
    }
    
    /**
     * @var array $requestData
     */
    public function execute($requestData)
    {
        try {
            $this->_validateRequestData($requestData);
        
            $settings = YMLSettingsDTO::fromArray(
                FileHelper::__parseFileContent(
                    YMLSettingsDTO::_getSettingsFile($requestData['id'])
                )
            );
            
            $this->_validateYMLSettings($settings);
            
            $catalogue = $this->___getCatalogueDTO();

            $output = $this->buildAction->run(
                $this->_getBuildCarsYMLInput($settings, $catalogue)
            );

            $this->_saveOutput($output);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    private function _saveOutput(BuildCarsYMLSuccessOutput $output)
    {
        $saveInput = new SaveYMLSettingsFileInput;
        $saveInput->settings = $output->settings;

        $this->saveAction->run($saveInput);
    }

    /**
     * @param YMLSettingsDTO $settings
     * @param CatalogueDTO $catalogue
     * 
     * @return BuildCarsYMLInput
     */
    private function _getBuildCarsYMLInput($settings, $catalogue)
    {
        $input = new BuildCarsYMLInput;
        
        $input->settings    = $settings;
        $input->catalogue   = $catalogue;

        return $input;
    }

    /**
     * @param YMLSettingsDTO $settings
     * 
     * @throws \Exception
     */
    private function _validateYMLSettings(YMLSettingsDTO $settings)
    {
        if (!$settings->id) {
            throw new \Exception('NO_YML_SETTINGS_FOR_THIS_ID_ERROR_CODE');
        }
        if (!$settings->categories) {
            throw new \Exception('NO_YML_CATEGORIES_IN_SETTINGS_ERROR_CODE');
        }
    }

    /**
     * @param array|mixed
     * 
     * @throws \Exception
     */
    private function _validateRequestData($requestData)
    {
        if (!$requestData['id']) {
            throw new \Exception('NO_YML_ID_ERROR_CODE');
        }
    }


    /**
     * @return CatalogueDTO
     * 
     * @throws \Exception
     */
    private function ___getCatalogueDTO()
    {
        $nc_catalogue = (\nc_Core::get_object())->catalogue->get_by_host_name(
            str_replace("www.", "", $_SERVER['HTTP_HOST'])
        );

        if (!$nc_catalogue) {
            throw new \Exception('NO_CATALOGUE_ERROR_CODE');
        }

        $catalogue          = new CatalogueDTO;
        $catalogue->id      = $nc_catalogue['Catalogue_ID'];
        $catalogue->name    = $nc_catalogue['Catalogue_Name'];
        $catalogue->domain  = $nc_catalogue['Domain'];

        return $catalogue;
    }
}