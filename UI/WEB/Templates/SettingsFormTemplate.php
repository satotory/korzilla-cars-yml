
<?php
use App\modules\Korzilla\YMLSection\CarsYML\Data\Constants\ListOfAvailableDefaultValues;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Constants\ListOfAvailablePatterns;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\CitiesCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\DefaultValuesCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\City;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\DefaultValue;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\Pattern;
use App\modules\Korzilla\YMLSection\CarsYML\Helpers\FileHelper;
use \App\modules\Korzilla\YMLSection\CarsYML\UI\WEB\Controllers\GetSettingsFormController;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;

global $nc_core;

/** @var GetSettingsFormController $controller */

$ymlID = $_GET['id'] ?? null;
if ($ymlID) {
    $settings = YMLSettingsDTO::fromArray(
        FileHelper::__parseFileContent(YMLSettingsDTO::_getSettingsFile($ymlID))
    );
} else {
    $settings = YMLSettingsDTO::fromArray(["id" => uniqid('cyi_')]);
}

?>

<form name="adminForm" 
    class="ajax2" 
    enctype="multipart/form-data" 
    method="post" 
    action="/bc/modules/Korzilla/YMLSection/CarsYML/UI/API/Routes/SaveYMLSettingsFileRoute.php">

    <div id="nc_moderate_form">
        <div class="nc_clear"></div>
        <?= $nc_core->token->get_input() ?>
        <input type="hidden" name="id" value="<?= ($settings->id) ?>">
    </div>


    <ul class="tabs tabs-border">
        <li class="tab"><a href="#tab_main">Главная</a></li>
        <li class="tab"><a href="#tab_categories">Разделы</a></li>
        <li class="tab"><a href="#tab_cities">Города</a></li>
        <li class="tab"><a href="#tab_patterns">Паттерны</a></li>
        <div class="t-border" style="left: 40px; width: 69px;"></div>
    </ul>

    <div class="modal-body tabs-body yml_export">

        <div id="tab_main">
            <div class='colblock'>
                <h4>Параметры по умолчанию</h4>
                <div class='multi-body'>
                    <?= _html_DefaultValuesList($settings->defaultValues->collection) ?>
                </div>
            </div>
        </div>
        <div id="tab_categories" class="none">
            <div class='sub-item-main'>
                <div class="sub-item-items" style='height:550px;'>
                    <ul id="categories_list" class="categories sub-item">
                        <?php /** @var \App\modules\Korzilla\YMLSection\CarsYML\Data\Models\SubdivisionModel $sub */ ?>
                        <?php foreach ($controller->_iterateSubs() as $sub) : ?>
                            <?php
                                $slashesCount   = (count(explode("/", trim($sub->Hidden_URL, "/"))) / 2);
                                $margin         = ($slashesCount == 1 ? 0 : floor($slashesCount)) * 15;
                                $checked = false;
                                if ($ymlID) {
                                    if (in_array($sub->Subdivision_ID, $settings->categories->collection)) {
                                        $checked = true;
                                    }
                                }
                            ?>
                            <?= bc_checkbox(
                                "categories[{$sub->Subdivision_ID}]", 
                                1, 
                                "<a href='{$sub->Hidden_URL}' target='_blank' alt='{$sub->Subdivision_Name}'>{$sub->Subdivision_Name}</a>",
                                $checked,
                                "data-parent='{$sub->Parent_Sub_ID}' style='margin-left:{$margin}px'"
                            ); ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div id="tab_cities" class="none">
            <?= _html_CitiesList($settings->cities->collection); ?>
        </div>
        <div id="tab_patterns" class="none">
            <?= _html_PatternsList($settings->patterns->collection); ?>
        </div>

    </div>
    <div class="bc_submitblock">
        <div class="result"></div>
        <span class="btn-strt"><input type="submit" value="Сохранить изменения">
        </span>
    </div>

</form>

<?php

/**
 * @param DefaultValue[]
 */
function _html_DefaultValuesList($dvArray) {
    $dvHTML = [];
    
    foreach ($dvArray as $key => $value) {
        $fieldKey = "defaultValues[{$key}][value]";
        if ($value->type == "input") {
            $field = bc_input($fieldKey, $value->value, $value->title ?? "");
        } else if ($value->type == 'select') {
            $field = bc_select(
                $fieldKey, 
                getOptionsFromArray(
                    $value->defaults, 
                    $value->value
                ),
                "",
                "class='ns'"
            );
        }
        if (!$field) {
            continue;
        }

        $dvHTML[] = sprintf("<div class='colline colline-1'>
                                <div class='colline colline-4'>%s</div>
                                <div class='colline colline-2'>%s</div>
                            </div>",
            bc_checkbox(
                "defaultValues[{$key}][active]", true, $value->name, $value->active
            ),
            $field
        );
    }

    return implode("", $dvHTML);
}

/**
 * @param Pattern[] $patternsArray
 */
function _html_PatternsList($patternsArray) {
    $patternsInputsHTML = [];

    foreach ($patternsArray as $key => $value) {

        $patternsInputsHTML[] = sprintf(
            "<div class='colline colline-1'><b>%s</b>%s</div>",
            $key,
            bc_input("patterns[{$key}][pattern]", $value->pattern)
        );
    }

    $availableSEOTagsHTML = [];
    foreach (ListOfAvailablePatterns::list() as $value) {
        $availableSEOTagsHTML[] = sprintf(
            "<li style='margin-left:20px'>> %s</li>",
            $value
        );
    }
    return sprintf("<div class='colline colline-1'>
                    <b>Список реализованных seo-слов</b>
                    <ul>%s</ul>
                </div>
                %s",
        implode("", $availableSEOTagsHTML),
        implode("", $patternsInputsHTML)
    );
}

/** 
 * @param City[] $citiesArray
 */
function _html_CitiesList($citiesArray)
{
    global $db, $catalogue, $setting;

    $citiesCheckboxesHTML = [];

    foreach ($setting['lists_targetcity'] as $city_id => $city) {
        $checked = $citiesArray[$city_id] ? true : false;

        $citiesCheckboxesHTML[] = sprintf(
            "<div class='colline colline-3'>%s</div>",
            bc_checkbox("cities[{$city_id}][id]", $city_id, $city['name'], $checked)
        );
    }
    
    return sprintf("<div class='colblock colblock-target'>
            <h4>Создание фид-файла для городов (без городов seo-паттерны будут проигнорированы)</h4>
            <div class='multi-body'>%s</div>
        </div>",
        implode("", $citiesCheckboxesHTML)
    );
}

?>