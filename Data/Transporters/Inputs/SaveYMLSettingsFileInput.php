<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Inputs;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;

class SaveYMLSettingsFileInput
{
    /**
     * @var YMLSettingsDTO
     */
    public $settings;
}