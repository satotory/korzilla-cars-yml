<?php declare(strict_types=1);

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Inputs;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\CatalogueDTO;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;

class BuildCarsYMLInput
{
    /**
     * @var CatalogueDTO
     */
    public $catalogue;

    /**
     * @var YMLSettingsDTO
     */
    public $settings;
}