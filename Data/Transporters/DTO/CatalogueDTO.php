<?php declare(strict_types=1);

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO;

class CatalogueDTO
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /**
     * @var string
     */
    public $domain;
}