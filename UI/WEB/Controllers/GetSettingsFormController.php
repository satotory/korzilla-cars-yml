<?php declare(strict_types=1);

namespace App\modules\Korzilla\YMLSection\CarsYML\UI\WEB\Controllers;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Repositories\SubdivisionRepository;

class GetSettingsFormController
{
    /** 
     * @var SubdivisionRepository
     */
    private $subdivisionRepository;

    /** 
     * @var int
     */
    private $catalogue_id;

    public function __construct()
    {
        $core   = \nc_Core::get_object();
        $db     = $core->db;
    
        $catalogue = $core->catalogue->get_by_host_name(
            str_replace("www.", "", $_SERVER['HTTP_HOST'])
        );

        $this->catalogue_id = $catalogue['Catalogue_ID'];

        $this->subdivisionRepository = new SubdivisionRepository($db);
    }

    public function execute() 
    {
        try {
            echo k_renderTemplate(
                '../Templates/SettingsFormTemplate.php', 
                [
                    'controller' => $this
                ]
            );
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function _iterateSubs()
    {
        return $this->subdivisionRepository->getProductSubdivisionsByCatalogueID_Iterable((int) $this->catalogue_id);
    }
}