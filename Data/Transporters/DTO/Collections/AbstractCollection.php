<?php declare(strict_types=1);

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections;

use Countable;
use Iterator;

abstract class AbstractCollection implements Iterator, Countable
{
    /**
     * @var array
     */
    public $collection = [];

    /**
     * @param mixed $item
     * @param mixed|null $key
     * 
     * @return void
     */
    public function add($item, $key = null)
    {
        if ($key === null) {
            $this->collection[] = $item;
        } else {
            $this->collection[$key] = $item;
        }
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return current($this->collection);
    }

    /**
     * @return void
     */
    public function next()
    {
        next($this->collection);
    }

    /**
     * @return int|string|null
     */
    public function key()
    {
        return key($this->collection);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return key($this->collection) !== null;
    }

    /**
     * @return void
     */
    public function rewind()
    {
        reset($this->collection);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->collection);
    }

    /**
     * @param string|int $key
     * 
     * @return mixed
     */
    public function get($key)
    {
        return $this->collection[$key] ?? null;
    }
}
