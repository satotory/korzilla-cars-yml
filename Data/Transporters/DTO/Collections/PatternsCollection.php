<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\Pattern;

class PatternsCollection extends AbstractCollection
{
    /**
     * @var Pattern[]
     */
    public $collection;

    /**
     * @param string|int $key
     * 
     * @return Pattern|null
     */
    public function get($key)
    {
        return $this->collection[$key] ?? null;
    }

    /**
     * @param array<array>|mixed $array
     */
    public function __construct($array)
    {
        foreach ($this->__requiredFields() as $key => $values) {
            $this->add(
                Pattern::fromArray($array[$key] ?? $values),
                $key
            );
        }
    }

    /**
     * @return array
     */
    private function __requiredFields()
    {
        return [
            'sets' => [
                'pattern' => '%CATEGORY%'
            ],
        ];
    }
}