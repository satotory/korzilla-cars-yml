<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures;

class Picture
{
    /**
     * @var string
     */
    public $path;
}