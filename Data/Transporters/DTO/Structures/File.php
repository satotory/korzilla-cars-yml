<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures;

class File
{
    /**
     * @var string полный путь
     */
    public $destination;
    
    /**
     * @var string относительный DOCUMENT_ROOT путь до файла
     */
    public $relativeDestination;

    /**
     * @var string
     */
    public $lastModified;

    /**
     * @var string контент файла
     */
    private $content;

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed
     * 
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }
}