<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures;
use App\modules\Korzilla\YMLSection\CarsYML\Helpers\CityHelper;

class City
{
    /**
     * @var int|string
     */
    public $id;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $keyword;

    public static function fromArray($data)
    {
        $city       = new City();
        if (!$data) {
            $city->id       = null;
            $city->name     = 'Без определенного города';
            $city->keyword  = 'default';
            
            return $city;
        }
        
        $city->id   = $data["id"];
        $city->name     = CityHelper::__getCityNameByCityId($city->id);
        $city->keyword  = CityHelper::__getCityKeywordByCityId($city->id);

        return $city;
    }
}