<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Helpers;

class LinksHelper
{
    /**
     * @param int|string $id
     */
    public static function __getProductURLById($id)
    {
        return nc_message_link($id, 2001);
    }
}