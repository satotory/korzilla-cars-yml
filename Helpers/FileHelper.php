<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Helpers;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;

class FileHelper
{
    /**
     * @return string
     */
    public static function __parseFileContent($filePath)
    {
        return orderArray(
            file_get_contents($filePath)
        );
    }

    /**
     * @return string
     */
    public static function __getFileContent($filePath)
    {
        return file_get_contents($filePath);
    }

    public static function __saveSettings(string $path, YMLSettingsDTO $settings)
    {
        $flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;

        return !!(
            file_put_contents(
                $path, 
                json_encode($settings, $flags))
        );
    }
}