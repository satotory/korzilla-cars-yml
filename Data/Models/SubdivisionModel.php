<?php declare(strict_types=1);

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Models;

class SubdivisionModel extends AbstractModel
{
    /**
     * @var int
     */
    public $Subdivision_ID;

    /**
     * @var string
     */
    public $Subdivision_Name;

    /**
     * @var string
     */
    public $Hidden_URL;

    /**
     * @var int
     */
    public $Parent_Sub_ID;

    /**
     * @var int
     */
    public $Products_Count;

    /**
     * @var string|null
     */
    private $set_guid = null;

    const SET_PREFIX = "set";
    /**
     * @return self
     */
    public function setSetGuid(int $id)
    {
        $this->set_guid = sprintf(
            "%s-%d",
            static::SET_PREFIX,
            $id
        );

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSetGuid()
    {
        return $this->set_guid ?? null;
    }
}