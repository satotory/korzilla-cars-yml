<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Repositories;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Models\CarModel;

class CarRepository extends AbstractRepository
{
    
    /**
     * @param int|string $subdivisionId
     * 
     * @return CarModel|array
     */
    public function getCarsByCategoryId_Iterable($subdivisionId)
    {
        $sql = sprintf(
            "SELECT *
                FROM %s
                WHERE (%s=%s OR INSTR(%s, %s)) 
                    AND %s > 0 
                GROUP BY %s",
            $this->getTableName(),
            $this->mapPropertyToDb('Subdivision_ID'),
            $this->mapValueToDb($subdivisionId),
            $this->mapPropertyToDb('Subdivision_IDs'),
            $this->mapValueToDb($subdivisionId),
            $this->mapPropertyToDb('price'),
            $this->mapPropertyToDb('Message_ID')
        );

        if (!$rows = $this->db->get_results($sql)) {
            var_dump($sql);
            var_dump($this->db->last_error);

            return [];
        }
        
        foreach ($rows as $row) {
            yield $this->mapRowToModel($row);
        }
    }

    protected function getModelClassName() : string
    {
        return CarModel::class;
    }

    protected function getTableName() : string
    {
        return 'Message2001';
    }
}