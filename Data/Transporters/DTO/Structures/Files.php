<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures;

class Files
{
    /**
     * @var File
     */
    public $patternFile;

    /**
     * @var YMLCityFile[]
     */
    public $ymlFiles;

    public function __construct($array)
    {
        $this->patternFile = new File;

        $this->patternFile->destination = $array['patternFile']['destination'];
        $this->patternFile->lastModified = $array['patternFile']['lastModified'];
        $this->patternFile->relativeDestination = $array['patternFile']['relativeDestination'];

        $this->ymlFiles = $this->_ymlFiles($array['ymlFiles']);
    }

    /**
     * @var array|mixed $data
     */
    private function _ymlFiles($data)
    {
        foreach ($data as $key => $value) {
            $ymlFiles[$key] = YMLCityFile::fromArray($value); 
        }

        return $ymlFiles;
    }
}