<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Actions\SubActions;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections\AbstractCollection;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;
use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Inputs\SaveYMLSettingsFileInput;
use App\modules\Korzilla\YMLSection\CarsYML\Helpers\FileHelper;

class SaveYMLSettingsSubAction
{
    public function run(SaveYMLSettingsFileInput $input)
    {
        try {
            if (!$this->_save($input->settings)) {
                throw new \Exception("SAVE_ERROR_CODE");
            }
        } catch (\Exception $e) {
            die("". $e->getMessage());
        }
    }

    /**
     * @return bool
     */
    private function _save(YMLSettingsDTO $settingsDTO)
    {
        $savingSettings = $settingsDTO;

        foreach ($savingSettings as $param => $value) {
            if (is_subclass_of($value, AbstractCollection::class, true)) {
                $savingSettings->$param = $value->collection;
            }
        }

        return FileHelper::__saveSettings(
            YMLSettingsDTO::_getSettingsFile($settingsDTO->id),
            $settingsDTO
        );
    }
}