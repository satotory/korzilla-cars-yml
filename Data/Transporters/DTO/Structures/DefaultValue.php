<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures;

class DefaultValue
{
    /**
     * @var bool
     */
    public $active;

    /**
     * @var string
     */
    public $name;

    /**
     * @var mixed
     */
    public $value;

    /**
     * @var mixed
     */
    public $defaults;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string|null
     */
    public $title;

    /**
     * @var array
     */
    public static function fromArray($data)
    {
        $dv = new DefaultValue();
        
        $dv->active     = (bool) $data["active"];
        $dv->name       = $data["name"];
        $dv->value      = $data["value"];
        $dv->type       = $data["type"];
        $dv->defaults   = $data["defaults"];
        $dv->title      = $data["title"] ?? null;

        return $dv;
    }
}