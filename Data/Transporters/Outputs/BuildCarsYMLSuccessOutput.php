<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\Outputs;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\YMLSettingsDTO;

class BuildCarsYMLSuccessOutput
{
    /**
     * @var YMLSettingsDTO
     */
    public $settings;
}