<?php

namespace App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Collections;

use App\modules\Korzilla\YMLSection\CarsYML\Data\Transporters\DTO\Structures\City;

class CitiesCollection extends AbstractCollection
{
    /**
     * @var City[]
     */
    public $collection;

    /**
     * @param array<array>|mixed $array
     */
    public function __construct($array)
    {
        foreach ($array as $key => $values) {
            $this->add(
                City::fromArray($values),
                (string) $key
            );
        }
    }
}